FROM openjdk:8-jre-alpine

ENV ACTIVEMQ_VERSION 5.15.14
ENV ACTIVEMQ_WORKDIR /app

RUN mkdir -p $ACTIVEMQ_WORKDIR

RUN wget -q -O ./apache-activemq-$ACTIVEMQ_VERSION-bin.tar.gz http://archive.apache.org/dist/activemq/$ACTIVEMQ_VERSION/apache-activemq-$ACTIVEMQ_VERSION-bin.tar.gz \
    && tar -xzf ./apache-activemq-$ACTIVEMQ_VERSION-bin.tar.gz -C $ACTIVEMQ_WORKDIR \
    && mv $ACTIVEMQ_WORKDIR/apache-activemq-$ACTIVEMQ_VERSION $ACTIVEMQ_WORKDIR/activemq \
    && rm -Rf apache-activemq-$ACTIVEMQ_VERSION.tar.gz

COPY lib $ACTIVEMQ_WORKDIR/activemq/lib

WORKDIR $ACTIVEMQ_WORKDIR/activemq

EXPOSE 8161
EXPOSE 61616

CMD ["/bin/sh", "-c", "bin/activemq console"]